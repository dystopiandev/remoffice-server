const sockio = require('socket.io')
const express = require('express')
const fs = require('fs')
const appConfig = require('./config')
const Database = require('./lib/db')
const log = require('./lib/log')
const blackbox = require('./blackbox/' + appConfig.blackboxDriver + '/driver.js')

// fancy boot tracking
const bootSteps = 3
var bootProgress = 0
const bootStatus = function (progress = true) {
  if (progress) bootProgress++
  return '[' + bootProgress + '/' + bootSteps + ']'
}

// kick-off
log.inverted('', appConfig.name, process.env.npm_package_version, '\n')

// init database
const db = new Database()

// open and wait for db
db.connect()
  .then((dbResponse) => {
    log.success(bootStatus(), dbResponse)

    // bootstrap webserver
    const app = express()
    if (fs.existsSync(appConfig.client.buildDir)) {
      app.use(express.static(appConfig.client.buildDir))
      log.success(bootStatus(), 'Web server started on', appConfig.socket.host + ':' + appConfig.socket.port, '(Internal Client: ON)')
    } else {
      log.success(bootStatus(), 'Web server started on', appConfig.socket.host + ':' + appConfig.socket.port, '(Internal Client: OFF)')
    }

    // bootstrap socket
    const io = sockio.listen(app.listen(appConfig.socket.port, appConfig.socket.host))
    log.success(bootStatus(), 'Socket server now listening on port', appConfig.socket.port)
    log.neutral('\n')

    // event watch: socket connection established
    io.sockets.on('connection', (socket) => {
      // announce client
      log.success('[' + socket.handshake.address + ']', '>', 'New socket handshake!')

      socket.on('getRooms', () => {
        log.info('[' + socket.handshake.address + ']', '>', 'Request: Rooms')
        db.fetchRooms()
          .then((rooms) => io.emit('rooms', rooms))
          .catch((err) => log.error(err))
      })

      socket.on('getCams', () => {
        log.info('[' + socket.handshake.address + ']', '>', 'Request: Cameras')
        db.fetchCams()
          .then((cams) => io.emit('cams', cams))
          .catch((err) => log.error(err))
      })

      socket.on('getSwitches', () => {
        log.info('[' + socket.handshake.address + ']', '>', 'Request: Switches')
        db.fetchSwitches()
          .then((switches) => io.emit('switches', switches))
          .catch((err) => log.error(err))
      })
    
      socket.on('getMasterSwitches', () => {
        log.info('[' + socket.handshake.address + ']', '>', 'Request: Master Switches')
        db.fetchMasterSwitches()
          .then((masterSwitches) => io.emit('masterSwitches', masterSwitches))
          .catch((err) => log.error(err))
      })
    
      socket.on('switchUpdate', (dataNode) => {
        db.updateSwitch(dataNode.id, dataNode.state ? 1 : 0)
          .then(() => {
            log.info('[' + socket.handshake.address + ']', '>', 'Toggle Switch', '#' + dataNode.id, dataNode.state ? 'ON' : 'OFF')
            db.fetchSwitches()
              .then((switches) => io.emit('switches', switches))
              .catch((err) => log.error(err))
          })
          .catch((err) => log.error(err))
      })
    
      socket.on('masterSwitchUpdate', (dataNode) => {
        db.updateMasterSwitch(dataNode.id, dataNode.state ? 1 : 0)
          .then(() => {
            log.info('[' + socket.handshake.address + ']', '>', dataNode.name, dataNode.state ? 'ON' : 'OFF')
            db.fetchMasterSwitches()
              .then((masterSwitches) => io.emit('masterSwitches', masterSwitches))
              .catch((err) => log.error(err))
          })
      })
    
      socket.on('rebootBlackbox', (delay) => {
        log.info('[' + socket.handshake.address + ']', '>', 'Rebooting Blackbox', (delay == 'now') ? 'immediately...' : 'in ' + delay + 's...')
        blackbox.reboot(delay)
          .then()
          .catch((err) => log.error(err))
      })
    
      socket.on('shutdownBlackbox', (delay) => {
        log.info('[' + socket.handshake.address + ']', '>', 'Shutting down Blackbox', (delay == 'now') ? 'immediately...' : 'in ' + delay + 's...')
        blackbox.shutdown(delay)
          .then()
          .catch((err) => log.error(err))
      })
    })
  })
  .catch((dbError) => {
    log.error(bootStatus(), dbError, '\n')
  })
