# Remoffice Server

> A server for IoT automations.

## Setup on Raspberry Pi

 1. Write Raspbian Stretch image to boot medium. Edit config.txt if necessary, then boot.

 2. Launch Pi config tool:

        sudo raspi-config

    - Boot Options > Desktop/CLI > Console Autologin

    - Boot Options > Wait for Network at Boot > Yes

    - Interfacing Options > SSH > Yes

    - If your local network is WiFi:
      
      Network Options > Wi-Fi
      
      Reboot OR ```wpa_cli -i wlan0 reconfigure```

    - Get server IP address -> ```ip addr | grep wlan0```

 3. SSH into Pi:

        ssh pi@<Server IP address>

    - password -> ```raspberry```

 4. Add a cron job:
 
        crontab -e

    -> ```@reboot /home/pi/Autorun.sh > /home/pi/AutorunLog.log 2>&1```

 5. Create auto-run shell script:
    
        nano /home/pi/Autorun.sh

    -> ```cd remoffice-server && npm start &```

 6. Make script executable:
    
        chmod +x /home/pi/Autorun.sh

 7. Install Node v8:
    
        curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
        sudo apt install -y nodejs
     
 8. Build and attach Remoffice Client:
     
    (Skip this step if you want to run in server-only mode. You must build and use external Remoffice Client instance(s) to interact with the server.)
    
        cd ../

        git clone https://github.com/r3dh4r7/remoffice-client

        cd remoffice-client

        npm install

        npm run build

 9. Fetch and bootstrap Remoffice Server:
    
        sudo apt install git

        git clone https://github.com/r3dh4r7/remoffice-server

        cd remoffice-server
        
        npm install

 10. Restart Pi:
     
         sudo shutdown -r now


## Connecting to the Server

The server runs on port 8088 and can be accessed via the blackbox's IP address from any device on the network by default.

Configure it to work as you want by editing the configuration file below.


## Server Configuration

The server configuration file is ```remoffice-server/config/index.js```

Default config:

```javascript
module.exports = {
  // Server name
  name: 'Remoffice Server',

  // Server verbosity
  enableLogs: true,

  // Database driver
  dbDriver: 'sqlite',

  // Blackbox config
  blackboxDriver:  'raspberry-pi-3',
  
  // Web socket interface
  socket: {
    host: '0.0.0.0',
    port: '8088'
  },

  // Database config
  db: {
    // SQLite
    sqlite: {
      index: 'database/sqlite/storage/index.sqlite'
    }
  },

  // Internal client build dir
  client: {
    buildDir: '../remoffice-client/dist'
  }
}
```
