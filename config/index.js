module.exports = {
  // Server name
  name: 'Remoffice Server',

  // Server verbosity
  enableLogs: true,

  // Database driver
  dbDriver: 'sqlite',

  // Blackbox config
  blackboxDriver:  'raspberry-pi-3',
  
  // Web socket interface
  socket: {
    host: '0.0.0.0',
    port: '8088'
  },

  // Database config
  db: {
    // SQLite
    sqlite: {
      index: 'database/sqlite/storage/index.sqlite'
    }
  },

  // Internal client build dir
   client: {
    buildDir: '../remoffice-client/dist'
  }
}
