const exec = require('child_process').exec

module.exports = {
  reboot (delay) {
    var cmd = 'sudo pkill node ; sudo pkill uv4l ; sudo shutdown -r now'
    cmd = (delay == 'now') ? cmd : 'sleep ' + delay + 's; ' + cmd
    
    return new Promise((resolve, reject) => {
      const rebootSequence = exec(cmd)
      rebootSequence.addListener('error', reject)
      rebootSequence.addListener('exit', resolve)
    })
  },

  shutdown (delay) {
    var cmd = 'sudo pkill node ; sudo pkill uv4l ; sudo shutdown now'
    cmd = (delay == 'now') ? cmd : 'sleep ' + delay + 's; ' + cmd

    return new Promise((resolve, reject) => {
      const rebootSequence = exec(cmd)
      rebootSequence.addListener('error', reject)
      rebootSequence.addListener('exit', resolve)
    })
  }
} 
